.. index::
   pair: Sphinx Theme; shibuya

.. _shibuya_thme:

==========================================================================================================
**shibuya** by Hsiaoming Yang (lepture)
==========================================================================================================

- https://github.com/lepture
- https://nitter.spaceint.fr/lepture/rss
- https://github.com/lepture/shibuya
- https://shibuya.lepture.com/


Used by
==========

authlib
-----------------

- https://docs.authlib.org/en/latest/index.html


jose (2023-06)
-----------------

- https://jose.authlib.org/en/dev/

I’m splitting modules from Authlib now. Currently working on
github.com/authlib/joserfc

It now contains all the features in authlib.jose module.
