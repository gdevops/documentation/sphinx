.. index::
   pair: Sphinx Theme; material

.. _ref_sphinx_material:

==========================================================================================================
**sphinx-material : A material-based, responsive theme inspired by mkdocs-material** by Kevin Sheppard
==========================================================================================================

- https://github.com/bashtage
- https://github.com/bashtage/sphinx-material/
- https://github.com/bashtage/sphinx-material/blob/master/docs/conf.py
- https://github.com/bashtage/sphinx-material/graphs/contributors
- https://sphinx-themes.org/sample-sites/sphinx-material/kitchen-sink/admonitions/

.. figure:: kevin_sheppard.png
   :align: center

   https://github.com/bashtage

Discovery
==========

- :ref:`material_2021_04_16`

Used by
==========

sphinx-material
-----------------

   - https://bashtage.github.io/sphinx-material

statsmodels
------------

- https://www.statsmodels.org/stable/index.html

arch
------

- https://bashtage.github.io/arch/

paperless-ngx
--------------

- https://docs.paperless-ngx.com/


tuto_sphinx_material
------------------------

- https://gdevops.gitlab.io/tuto_sphinx_material/
