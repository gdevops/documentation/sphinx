.. index::
   pair: Sphinx Theme; furo

.. _furo_theme:

=============================
**sphinx furo theme**
=============================

- https://github.com/pradyunsg/furo
- https://pradyunsg.me/furo/quickstart/
- https://mastodon.social/@pradyunsg

Useb by
=========

- https://urllib3.readthedocs.io/en/latest/
- https://docs.aiven.io, https://github.com/aiven/devportal/blob/main/conf.py
- https://tox.wiki/en/latest/, https://github.com/tox-dev/tox/blob/main/docs/conf.py
- https://kdeldycke.github.io/click-extra/sphinx.html, https://github.com/kdeldycke/click-extra/blob/main/docs/conf.py
