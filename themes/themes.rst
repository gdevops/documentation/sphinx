.. index::
   pair: Sphinx; Themes

.. _sphinx_themes:

============================================================
Sphinx themes
============================================================

- https://awesomesphinx.useblocks.com/categories/themes.html
- http://sphinx-doc.org/theming.html
- https://sphinx-themes.github.io/sphinx-themes.org/
- https://x.com/sphinxthemes


.. toctree::
   :maxdepth: 3

   furo/furo
   sphinx-immaterial/sphinx_immaterial.rst
   sphinx_material/sphinx_material
   pydata-sphinx-theme/pydata-sphinx-theme
   sphinx_book_theme/sphinx_book_theme
   shibuya/shibuya


.. https://github.com/zclab/stmaterial
.. https://openforcefield.github.io/openff-sphinx-theme/dev/index.html
.. - https://xanadu-sphinx-theme.readthedocs.io/en/latest/index.html
.. https://sphinx-artisan-theme.artisan.io/installation/
.. https://sphinxdocs.ansys.com/version/stable/index.html
.. https://sphinxawesome.xyz/how-to/add/
.. https://github.com/mgeier/insipid-sphinx-theme
.. https://github.com/saeiddrv/SphinxMinooTheme (persan)
.. https://github.com/pradyunsg/lutra
.. https://typo3-documentation.github.io/sphinx_typo3_theme/
.. https://sphinx-wagtail-theme.readthedocs.io/en/latest/index.html
