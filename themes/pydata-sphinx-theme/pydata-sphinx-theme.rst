
.. index::
   pair: Sphinx; pydata_sphinx_theme
   pair: Sphinx; pydata_sphinx_theme

.. _pydata_sphinx_theme:

======================================================================================================================
**pydata-sphinx-theme** (very good, A clean, three-column Sphinx theme with Bootstrap for the PyData community )
======================================================================================================================

- https://pydata-sphinx-theme.readthedocs.io/en/stable/index.html
- https://github.com/pandas-dev/pydata-sphinx-theme
- https://github.com/pandas-dev/pydata-sphinx-theme/graphs/contributors
- https://pydata-sphinx-theme.readthedocs.io/en/latest/
- https://x.com/pandas_dev
- https://pandas.pydata.org/

Gallery
=========

- https://pydata-sphinx-theme.readthedocs.io/en/latest/examples/gallery.html

predictablynoisy
--------------------

- https://predictablynoisy.com/posts/2020/sphinx-blogging/


geopandas
------------

- :ref:`geopandas`
