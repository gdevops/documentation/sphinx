
.. _sphinx_book_theme:

=================================================================================
|eb| **sphinx_book_theme**
=================================================================================

- https://github.com/executablebooks/sphinx-book-theme
- https://sphinx-book-theme.readthedocs.io/en/latest/

Used by
==========


Tuto documentation
---------------------

- https://gdevops.gitlab.io/tuto_documentation/meta/meta.html#sphinx-theme-sphinx-book-theme


Installation
==============

::

    poetry add sphinx_book_theme


::

    html_theme = "sphinx_book_theme"

::

    liste_full = [
        "globaltoc.html",
        "postcard.html",
        "recentposts.html",
        "sourcelink.html",
        "archives.html",
        "tagcloud.html",
        "categories.html",
        "searchbox.html",
    ]
