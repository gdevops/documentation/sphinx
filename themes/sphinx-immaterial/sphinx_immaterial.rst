.. index::
   pair: Sphinx Theme; sphinx-immaterial
   ! immaterial

.. _ref_sphinx_immaterial:

=======================================================================================================================================================
**sphinx-immaterial : Adaptation of the popular mkdocs-material material design theme to the sphinx documentation system** by Jeremy Maitin-Shepard
=======================================================================================================================================================

- https://github.com/jbms/sphinx-immaterial
- https://jbms.github.io/sphinx-immaterial/



Used by
===========

- https://github.com/useblocks/sphinx-needs
