.. index::
   pair: sphinx ; tools for sphinx
   pair: Qt applis ; tools for sphinx


.. _tools_for_sphinx:

===============================
Tools for building sphinx
===============================

- https://pythonawesome.com/a-curated-list-of-awesome-tools-for-sphinx-python-documentation-generator/
- https://github.com/yoloseem/awesome-sphinxdoc

.. toctree::
   :maxdepth: 3

   qt_program
   bash_program
   rst_lint
