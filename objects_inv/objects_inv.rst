.. index::
   ! objects.inv


.. _objects_inv:

===================================
**objects.inv**
===================================

- https://github.com/sphinx-doc/sphinx/tree/master/sphinx/ext
- https://www.sphinx-doc.org/en/master/usage/extensions/index.html

Calling examples
==================

::

    python -m sphinx.ext.intersphinx https://gdevops.frama.io/opsindev/tuto-project/objects.inv

sphinx.ext.intersphinx documentation #6911
==============================================

.. seealso::

   - https://github.com/sphinx-doc/sphinx/issues/6911

A documentation generated with Sphinx generated also a so called inventory.

For example, all documentation on ReadTheDocs have such inventory files.
This is the target for intersphinx.

If the inventory name and path is the default, you can provide None as
a parameter to the Intersphinx configuration.

Here is a configuration from one of my projects::

    # Sphinx.Ext.InterSphinx
    intersphinx_mapping = {
        'python':         ('https://docs.python.org/3', None),
        'pyExceptions':   ('http://pyExceptions.readthedocs.io/en/latest', None),
        'pyMetaClasses':  ('http://pyMetaClasses.readthedocs.io/en/latest', None),
    }

Other examples
===================

::

    intersphinx_mapping = {
        "sphinx": ("https://www.sphinx-doc.org/en/master", None),
        "sphinx_material": ("https://gdevops.gitlab.io/tuto_sphinx_material", None),
        "python": ("https://docs.python.org/", None),
        "tuto_python": ("https://gdevops.gitlab.io/tuto_python/", None),
        "tuto_django": ("https://gdevops.frama.io/django/tuto/", None),
        "til": ("https://gdevops.gitlab.io/today_i_learned/", None),
        "blockdiag": ("http://blockdiag.com/en/", None),
    }
