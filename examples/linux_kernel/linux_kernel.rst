

.. index::
   pair: Sphinx ; Linux kernel

.. _linux_kernel:

=============================
linux kernel
=============================

.. seealso::

   - https://www.kernel.org/doc/html/latest/doc-guide/sphinx.html
   - https://lwn.net/Articles/692704/





Introduction
=============


The Linux kernel uses `Sphinx`_ to generate pretty documentation from
`reStructuredText`_ files under ``Documentation``. To build the documentation in
HTML or PDF formats, use ``make htmldocs`` or ``make pdfdocs``. The generated
documentation is placed in ``Documentation/output``.

.. _Sphinx: http://www.sphinx-doc.org/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html

The reStructuredText files may contain directives to include structured
documentation comments, or kernel-doc comments, from source files. Usually these
are used to describe the functions and types and design of the code. The
kernel-doc comments have some special structure and formatting, but beyond that
they are also treated as reStructuredText.

Finally, there are thousands of plain text documentation files scattered around
``Documentation``. Some of these will likely be converted to reStructuredText
over time, but the bulk of them will remain in plain text.


Sphinx-doc extensions for sophisticated C developer
=====================================================

.. seealso:: https://github.com/return42/linuxdoc
