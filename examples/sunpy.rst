.. index::
   pair: projects using sphinx ; sunpy

.. _sunpy_doc:

=================================
sunpy Documentation
=================================


.. seealso::

   - https://github.com/sunpy/sunpy/tree/master/docs
   - https://github.com/sunpy/sunpy/blob/master/docs/conf.py
   - https://docs.sunpy.org/en/stable/
