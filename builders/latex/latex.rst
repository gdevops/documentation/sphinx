.. index::
   pair: Sphinx ; Latex Builders
   pair: PDF ; LateX
   pair: MiKTeX ; Docker
   pair: MiKTeX ; LateX
   pair: Docker ; docker-sphinx

.. _sphinx_latex:

=======================
Sphinx LateX builders
=======================


.. seealso::

   - http://www.sphinx-doc.org/en/master/builders.html#builders




LaTeXBuilder
=============

This builder produces a bunch of LaTeX files in the output directory.

You have to specify which documents are to be included in which LaTeX
files via the latex_documents configuration value.

There are a few configuration values that customize the output of this
builder, see the chapter Options for LaTeX output for details.

The produced LaTeX file uses several LaTeX packages that may not be
present in a “minimal” TeX distribution installation.

For example, on Ubuntu, the following packages need to be installed
for successful PDF builds:

- texlive-latex-recommended
- texlive-fonts-recommended
- texlive-latex-extra
- latexmk (for make latexpdf on GNU/Linux and MacOS X)
- latex-xcolor (old Ubuntu)
- texlive-luatex, texlive-xetex (see latex_engine)

The testing of Sphinx LaTeX is done on Ubuntu trusty with the above
mentioned packages.


Issues
--------

The font “FreeSerif” cannot be found
+++++++++++++++++++++++++++++++++++++++

.. seealso::

   - https://tex.stackexchange.com/questions/546246/centos-8-the-font-freeserif-cannot-be-found



I've hit the same wall. Fortunately, datakid showed the way in the comments::

    changing the latex engine back to the default in Sphinx

To do so, I removed this line from the conf.py:

::

    latex_engine = 'lualatex'

In my case, I still wanted to use LuaLaTeX to build the docs.
This is what I did::

    sphinx-build -b latex . _build/pdf
    cd _build/pdf
    make LATEXMKOPTS="-lualatex"

I hope this helps!




MiKTeX installation
=====================


Dockerized MiKTeX
------------------

.. seealso::

   - https://miktex.org/howto/miktex-docker


The Docker image allows you to run MiKTeX on any computer that supports
Docker.

Obtaining the image
+++++++++++++++++++++

Get the latest image from the registry::

    docker pull miktex/miktex

Using the image
+++++++++++++++++

Prerequisites

The host directory containing the input files must be mounted to the
container path /miktex/work.

The container image contains a bare MiKTeX setup and MiKTeX is configured
to install missing files the container directory /miktex/.miktex.
It is recommended that you mount this directory to a Docker volume.


docker-sphinx
--------------

.. seealso::

   - https://hub.docker.com/r/plaindocs/docker-sphinx/~/dockerfile/
   - https://github.com/plaindocs/docker-sphinx
