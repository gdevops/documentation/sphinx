
.. _sphinx_5_0_0_beta1:

================================================
Release 5.0.0 beta1 (released May 09, 2022)
================================================

- https://github.com/sphinx-doc/sphinx/releases/tag/v5.0.0b1
- https://www.sphinx-doc.org/en/master/changes.html#release-5-0-0-beta1-released-may-09-2022
- https://mail.python.org/archives/list/python-announce-list@python.org/thread/4FCYJ437A5GAUI27WCQPVI5E2WVXM2ID/
- https://github.com/sphinx-doc/sphinx/blob/5.0.x/CHANGES


Dependencies
==============


* #10164: Support `Docutils 0.18`_. Patch by Adam Turner.

.. _Docutils 0.18:  https://docutils.sourceforge.io/RELEASE-NOTES.html#release-0-18-2021-10-26

Incompatible changes
======================

* #10031: autosummary: ``sphinx.ext.autosummary.import_by_name()`` now raises
  ``ImportExceptionGroup`` instead of ``ImportError`` when it failed to import
  target object.  Please handle the exception if your extension uses the
  function to import Python object.  As a workaround, you can disable the
  behavior via ``grouped_exception=False`` keyword argument until v7.0.
* #9962: texinfo: Customizing styles of emphasized text via ``@definfoenclose``
  command was not supported because the command was deprecated since texinfo 6.8
* #2068: :confval:`intersphinx_disabled_reftypes` has changed default value
  from an empty list to ``['std:doc']`` as avoid too surprising silent
  intersphinx resolutions.
  To migrate: either add an explicit inventory name to the references
  intersphinx should resolve, or explicitly set the value of this configuration
  variable to an empty list.
* #10197: html theme: Reduce ``body_min_width`` setting in basic theme to 360px
* #9999: LaTeX: separate terms from their definitions by a CR (refs: #9985)
* #10062: Change the default language to ``'en'`` if any language is not set in
  ``conf.py``

Features added
=================

* #9075: autodoc: The default value of :confval:`autodoc_typehints_format` is
  changed to ``'smart'``.  It will suppress the leading module names of
  typehints (ex. ``io.StringIO`` -> ``StringIO``).
* #8417: autodoc: ``:inherited-members:`` option now takes multiple classes.  It
  allows to suppress inherited members of several classes on the module at once
  by specifying the option to :rst:dir:`automodule` directive
* #9792: autodoc: Add new option for ``autodoc_typehints_description_target`` to
  include undocumented return values but not undocumented parameters.
* #10285: autodoc: singledispatch functions having typehints are not documented
* autodoc: :confval:`autodoc_typehints_format` now also applies to attributes,
  data, properties, and type variable bounds.
* #10258: autosummary: Recognize a documented attribute of a module as
  non-imported
* #10028: Removed internal usages of JavaScript frameworks (jQuery and
  underscore.js) and modernised ``doctools.js`` and ``searchtools.js`` to
  EMCAScript 2018.
* #10302: C++, add support for conditional expressions (``?:``).
* #5157, #10251: Inline code is able to be highlighted via :rst:dir:`role`
  directive
* #10337: Make sphinx-build faster by caching Publisher object during build
