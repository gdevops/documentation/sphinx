.. index::
   pair: Sphinx ; 5.0.0 (2022-06-16)

.. _sphinx_5.0.0:

===============================
Sphinx **5.0.0 (2022-06-16)**
===============================

- https://github.com/sphinx-doc/sphinx/blob/master/CHANGES


.. toctree::
   :maxdepth: 3

   beta1/beta1
