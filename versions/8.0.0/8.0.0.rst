.. index::
   pair: Sphinx ; 8.0.0 (2024-07-29)

.. _sphinx_8_0_0:

===============================
**Sphinx 8.0.0** (2024-07-29)
===============================

- https://github.com/orgs/sphinx-doc/discussions/12709
- https://github.com/sphinx-doc/sphinx/releases/tag/v8.0.0
- https://www.sphinx-doc.org/en/master/changes/8.0.html
