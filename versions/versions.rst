.. index::
   pair: Sphinx ; versions

.. _sphinx_versions:

=======================
Sphinx versions
=======================

- https://github.com/sphinx-doc/sphinx/graphs/contributors
- https://github.com/orgs/sphinx-doc/discussions/categories/project-announcements
- https://github.com/sphinx-doc/sphinx/tags
- http://www.sphinx-doc.org/en/master/changes.html

.. toctree::
   :maxdepth: 4

   8.2.0/8.2.0
   8.0.0/8.0.0
   7.4.3/7.4.3
   7.3.5/7.3.5
   7.3.0/7.3.0
   7.0.0/7.0.0
   6.0.0/6.0.0
   5.1.0/5.1.0
   5.0.0/5.0.0
   4.2.0/4.2.0
   4.1.2/4.1.2
   4.1.0/4.1.0
   4.0.0/4.0.0
   3.4.0/3.4.0
   3.3.0/3.3.0
   3.2.1/3.2.1
   3.0.0/3.0.0
   2.4.0/2.4.0
   2.3.0/2.3.0
   2.2.0/2.2.0
   2.1.0/2.1.0
   2.0.0/2.0.0
   1.6.3
   1.2.3
   1.1.1
   1.1
