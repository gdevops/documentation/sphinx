.. index::
   pair: Sphinx ; 3.2.1 (2020-08-14)

.. _sphinx_3.2.1:

===============================
Sphinx **3.2.1 (2020-08-14)**
===============================


.. seealso::

   - https://github.com/sphinx-doc/sphinx/blob/master/CHANGES
   - https://github.com/sphinx-doc/sphinx/releases/tag/v3.2.1
   - https://github.com/sphinx-doc/sphinx/tree/v3.2.1
   - https://github.com/sphinx-doc/sphinx/blob/v3.2.1/CHANGES





Features added
=================

* #8095: napoleon: Add `napoleon_preprocess_types` to enable the type
  preprocessor for numpy style docstrings
* #8114: C and C++, parse function attributes after parameters and qualifiers.

Bugs fixed
============

* #8074: napoleon: Crashes during processing C-ext module
* #8088: napoleon: "Inline literal start-string without end-string" warning in
  Numpy style Parameters section
* #8084: autodoc: KeyError is raised on documenting an attribute of the broken
  class
* #8091: autodoc: AttributeError is raised on documenting an attribute on Python
  3.5.2
* #8099: autodoc: NameError is raised when target code uses ``TYPE_CHECKING``
* C++, fix parsing of template template paramters, broken by the fix of #7944
