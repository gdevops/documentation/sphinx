.. index::
   pair: Sphinx ; 7.0.0 (2023-04-29)

.. _sphinx_7.0.0:

===============================
**Sphinx 7.0.0** (2023-04-29)
===============================


- https://www.sphinx-doc.org/en/master/changes.html#release-7-0-0-released-apr-29-2023
- https://github.com/sphinx-doc/sphinx/commit/d568b2f4f7cca743fcbf70814d15602d8129b790
- https://github.com/AA-Turner
