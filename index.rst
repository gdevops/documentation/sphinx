.. index::
   ! Sphinx
   pair: Documentation ; Sphinx

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

.. https://github.com/owner/repo/releases.atom
.. https://github.com/owner/repo/commits.atom
.. https://github.com/user.atom


|FluxWeb| `RSS <https://gdevops.frama.io/documentation/sphinx/rss.xml>`_

.. _documenting_with_sphinx:
.. _tuto_sphinx:

===================================
**Sphinx** documentation builder
===================================

- https://github.com/sphinx-doc/sphinx
- https://github.com/sphinx-doc/sphinx/graphs/contributors
- https://github.com/orgs/sphinx-doc/discussions
- http://www.sphinx-doc.org/en/master/
- https://en.wikipedia.org/wiki/Sphinx_(documentation_generator)
- https://pradyunsg.me/furo/reference/
- http://rest-sphinx-memo.readthedocs.org/en/latest/Project.html
- http://rest-sphinx-memo.readthedocs.org/en/latest/References.html
- http://redaction-technique.org/index.html
- https://github.com/sphinx-doc/sphinx/graphs/contributors

.. figure:: sphinx.png
   :align: center

   *Sphinx logo*


- http://blog.smartbear.com/software-quality/bid/256072/13-reasons-your-open-source-docs-make-people-want-to-scream
- http://brikis98.blogspot.de/2014/05/you-are-what-you-document.html
- http://redaction-technique.org/index.html


Our favorite document generator is :ref:`sphinx <tuto_sphinx>` and we use the
:ref:`reStructuredText markup <doc_formats:rest_sphinx_doc>` and :ref:`sphinx-design <sphinx_design>`

..  epigraph::

    I'm not impressed if you're a good engineer. I'm impressed if you're a
    good teammate.

    https://x.com/RandallKanna/status/1387202807103574016?s=20



Description
============

**Sphinx** is a tool that makes it easy to create intelligent and beautiful
documentation, written by Georg Brandl and licensed under the BSD license.

It was originally created for the new Python documentation, and it has excellent
facilities for the documentation of Python projects, but C/C++ is already
supported as well, and it is planned to add special support for other languages
as well.

Sphinx source code (Sphinx dev guide)
=====================================

- https://github.com/sphinx-doc/sphinx


.. toctree::
   :maxdepth: 5

   people/people
   development/development


Sphinx applications
===================

.. toctree::
   :maxdepth: 4

   applications/applications


Sphinx extensions **(sphinx.ext.\*)**
=======================================

.. toctree::
   :maxdepth: 4

   extensions/extensions


Sphinx contributed extensions
==============================

.. toctree::
   :maxdepth: 4

   contributed_extensions/contributed_extensions


Sphinx howto
===================

.. toctree::
   :maxdepth: 4

   howto/howto
   for_apis/for_apis


Sphinx deployment
===================

.. toctree::
   :maxdepth: 4

   deployment/deployment

Sphinx examples
=====================

.. toctree::
   :maxdepth: 4

   examples/examples

Sphinx i18n
===================

.. toctree::
   :maxdepth: 4

   i18n/i18n

Sphinx builders
===================

.. toctree::
   :maxdepth: 4

   builders/builders


Sphinx installation
===================

.. toctree::
   :maxdepth: 4

   install/install

Sphinx objects.inv (inventory)
==============================

.. toctree::
   :maxdepth: 4

   objects_inv/objects_inv

Sphinx usage
===================

.. toctree::
   :maxdepth: 4

   usage/usage


Sphinx tutorials
===================

.. toctree::
   :maxdepth: 4

   tutorials/tutorials


Tools for Sphinx
===================

.. toctree::
   :maxdepth: 4

   tools_for_sphinx/tools_for_sphinx


Sphinx themes
===================

.. toctree::
   :maxdepth: 4

   themes/themes


Sphinx templating
===================

.. toctree::
   :maxdepth: 4

   templating/templating


Sphinx translations
===================

.. toctree::
   :maxdepth: 4

   translations/translations

Sphinx versions
===================

.. toctree::
   :maxdepth: 4

   versions/versions
