
.. index::
   pair: installation ; projet sphinx

===============================
Installation d'un projet sphinx
===============================

.. seealso::

   - http://sphinx-doc.org/latest/index.html
   - http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/sphinx.html





Installation d'une documentation Sphinx 'reStructuredText' sous Windows
=======================================================================

.. seealso::

   - http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/sphinx.html


#. ouvrir une fenêtre de commande
    .. image:: open_command.png
       :alt: Ouverture d'une fenêtre de commande.


#. taper les commandes suivantes

    ::

        > cd meta
        > sphinx-quickstart

    .. image:: sphinx_quickstart.png
       :alt: Construction du projet sphinx.


Production de la documentation html
===================================

#. taper la commande suivante
    ::

        > make html

   .. image:: make_html.png
      :alt: Production de la documentation html.



Exemples d'installation
========================

.. toctree::
   :maxdepth: 3

   exemples/exemples
