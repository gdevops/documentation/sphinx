

.. index::
   pair: Sphinx ; Jupyter
   pair: Sphinx ; Notebook


.. _sphinx_notebook:

=======================
Sphinx usage notebook
=======================




.. _practical_sphinx_notebook:

Practical sphinx
==================

.. seealso:: https://speakerdeck.com/willingc/practical-sphinx


Jupyter/notebook
------------------

.. code-block:: python
   :linenos:

   extensions = [
       'sphinx.ext.autodoc',
       'sphinx.ext.doctest',
       'sphinx.ext.intersphinx',
       'sphinx.ext.autosummary',
       'sphinx.ext.mathjax',
       'nbsphinx',
   ]

   # Add type of source files
   source_suffix = ['.rst', '.md', '.ipynb']
