

.. index::
   pair: MyST ; Markdown

.. _sphinx_mardown:

=======================================
Sphinx usage markdown (=> use MyST**)
=======================================

- http://www.sphinx-doc.org/en/master/usage/markdown.html
- https://github.com/readthedocs/recommonmark/issues/221
- :ref:`doc_formats:myst_parser`
- :ref:`doc_formats:markdown_it_py`


2021-05-05
============

- https://blog.readthedocs.com/newsletter-may-2021/

After some careful deliberation, we started the process to `deprecate
recommonmark <https://github.com/readthedocs/recommonmark/issues/221>`_
**in favor of MyST**, a better maintained alternative.

We are excited to see that some projects have already migrated without
effort, and we are looking forward to helping MyST-Parser thrive!

Thanks to the Executable Books Project folks for creating this project.

