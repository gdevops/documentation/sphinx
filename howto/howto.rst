.. index::
   pair: Sphinx ; Howto
   pair: Sphinx ; pipenv
   pair: Sphinx ; .venv exclude_patterns
   pair: .venv ; exclude_patterns

.. _sphinx_howto:

=======================
Sphinx howto
=======================

How to exclude some files or directories ?
=============================================

Use exclude_patterns in your :file:`conf.py` file.

If you want to exclude the .venv directory produced by
pipenv when using **PIPENV_VENV_IN_PROJECT=1**


::

	# List of patterns, relative to source directory, that match files and
	# directories to ignore when looking for source files.
	# This pattern also affects html_static_path and html_extra_path .
	exclude_patterns = [
		'_build',
		'Thumbs.db',
		'.DS_Store',
		# pipenv virtualenv with PIPENV_VENV_IN_PROJECT=1
		'.venv'
	]
