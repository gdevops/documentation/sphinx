.. index::
   pair: sphinx extension; sphinx.ext.autodoc.typehints
   ! sphinx.ext.autodoc.typehints
   ! Typing

.. _sphinx_ext_autodoc_typehints:

============================================================================================
**sphinx.ext.autodoc.typehints** (**autodoc_typehints**)  (since sphinx 2.4.0, 2020-02-09)
============================================================================================

.. seealso::

   - https://github.com/sphinx-doc/sphinx/blob/master/sphinx/ext/autodoc/typehints.py
   - https://tk0miya.hatenablog.com/entry/2020/02/09/190523
   - :ref:`sphinx_ext_autodoc`
   - :ref:`sphinx_2_4_0`


Generating documents from type annotations
===========================================

As an **experimental feature**, autodoc provides *sphinx.ext.autodoc.typehints* as
an additional extension.

It extends autodoc itself to generate function document from its type annotations.

To enable the feature, please add **sphinx.ext.autodoc.typehints** to list
of extensions and set ‘description’ to autodoc_typehints::

    extensions = ['sphinx.ext.autodoc', 'sphinx.ext.autodoc.typehints']


autodoc_typehints = 'description'

New in :ref:`version 2.4 <sphinx_2_4_0>` : Added as an experimental feature.

This will be integrated into autodoc core in Sphinx-3.0.


autodoc_typehints
====================

- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/#autodoc_typehints

**This is technically an autodoc setting, but Sphinx AutoAPI honours it**.

It controls if/where type hints are included in the documentation.

The possible values are the following:

- "signature": type hints are included in the function signature, which
  appears first in the member’s documentation
- "description": type hints are included within the function description,
  when the arguments are listed
- "both": type hints are included in both places
- "none": type hints are not included

My preference goes for "signature"::

    autodoc_typehints = "signature"
