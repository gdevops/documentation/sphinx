.. index::
   pair: Sphinx ; Extensions
   ! Extensions


.. _sphinx_extensions:

===================================
**sphinx.ext.** Sphinx extensions
===================================

- https://github.com/sphinx-doc/sphinx/tree/master/sphinx/ext
- https://www.sphinx-doc.org/en/master/usage/extensions/index.html

.. toctree::
   :maxdepth: 3


   sphinx.ext.autodoc/sphinx.ext.autodoc
   sphinx.ext.autodoc.typehints/sphinx.ext.autodoc.typehints
   sphinx.ext.autosummary/sphinx.ext.autosummary
   sphinx.ext.intersphinx/sphinx.ext.intersphinx
   sphinx.ext.napoleon/sphinx.ext.napoleon
