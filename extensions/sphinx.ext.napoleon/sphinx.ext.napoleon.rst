.. include:: ../../sphinx_refs.txt

.. index::
   pair: Parameters; Napoleon
   ! Napoleon
   ! sphinx.ext.napoleon


.. _sphinx_ext_napoleon:

===============================================================================
**sphinx.ext.napoleon** (Support for **NumPy and Google** style docstrings)
===============================================================================

.. seealso::

   - https://github.com/sphinx-doc/sphinx/tree/master/sphinx/ext/napoleon
   - http://sphinxcontrib-napoleon.readthedocs.org
   - https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
   - :ref:`sphinx_2_4_0`




Description
============


Are you tired of writing docstrings that look like this::

    :param path: The path of the file to wrap
    :type path: str
    :param field_storage: The :class:`FileStorage` instance to wrap
    :type field_storage: FileStorage
    :param temporary: Whether or not to delete the file when the File
       instance is destructed
    :type temporary: bool
    :returns: A buffered writable file descriptor
    :rtype: BufferedFileStorage

`ReStructuredText`_ is great, but it creates visually dense, hard to read
`docstrings`_. Compare the jumble above to the same thing rewritten
according to the `Google Python Style Guide`_::

    Args:
        path (str): The path of the file to wrap
        field_storage (FileStorage): The :class:`FileStorage` instance to wrap
        temporary (bool): Whether or not to delete the file when the File
           instance is destructed

    Returns:
        BufferedFileStorage: A buffered writable file descriptor

Much more legible, no?

Napoleon is a `Sphinx extension`_ that enables Sphinx to parse both `NumPy`_
and `Google`_ style docstrings - the style recommended by `Khan Academy`_.

Napoleon is a pre-processor that parses `NumPy`_ and `Google`_ style
docstrings and converts them to reStructuredText before Sphinx attempts to
parse them. This happens in an intermediate step while Sphinx is processing
the documentation, so it doesn't modify any of the docstrings in your actual
source code files.


Getting Started
=================

1. After `setting up Sphinx`_ to build your docs, enable napoleon in the
   Sphinx `conf.py` file::

       # conf.py

       # Add autodoc and napoleon to the extensions list
       extensions = ['sphinx.ext.autodoc', 'sphinx.ext.napoleon']

2. Use `sphinx-apidoc` to build your API documentation::

       $ sphinx-apidoc -f -o docs/source projectdir

.. _setting up Sphinx: http://sphinx-doc.org/tutorial.html

Docstrings
============

Napoleon interprets every docstring that `Sphinx autodoc`_ can find,
including docstrings on: ``modules``, ``classes``, ``attributes``,
``methods``, ``functions``, and ``variables``. Inside each docstring,
specially formatted `Sections`_ are parsed and converted to
reStructuredText.

All standard reStructuredText formatting still works as expected.

.. _Sphinx autodoc: http://sphinx-doc.org/ext/autodoc.html


.. _Sections:

Docstring Sections
====================

All of the following section headers are supported:

    * ``Args`` *(alias of Parameters)*
    * ``Arguments`` *(alias of Parameters)*
    * ``Attributes``
    * ``Example``
    * ``Examples``
    * ``Keyword Args`` *(alias of Keyword Arguments)*
    * ``Keyword Arguments``
    * ``Methods``
    * ``Note``
    * ``Notes``
    * ``Other Parameters``
    * ``Parameters``
    * ``Return`` *(alias of Returns)*
    * ``Returns``
    * ``Raises``
    * ``References``
    * ``See Also``
    * ``Warning``
    * ``Warnings`` *(alias of Warning)*
    * ``Warns``
    * ``Yields``

Google vs NumPy
================

Napoleon supports two styles of docstrings: `Google`_ and `NumPy`_. The
main difference between the two styles is that Google uses indention to
separate sections, whereas NumPy uses underlines.

Google style::

    def func(arg1, arg2):
        """Summary line.

        Extended description of function.

        Args:
            arg1 (int): Description of arg1
            arg2 (str): Description of arg2

        Returns:
            bool: Description of return value

        """
        return True

NumPy style::

    def func(arg1, arg2):
        """Summary line.

        Extended description of function.

        Parameters
        ----------
        arg1 : int
            Description of arg1
        arg2 : str
            Description of arg2

        Returns
        -------
        bool
            Description of return value

        """
        return True

NumPy style tends to require more vertical space, whereas Google style
tends to use more horizontal space. Google style tends to be easier to
read for short and simple docstrings, whereas NumPy style tends be easier
to read for long and in-depth docstrings.

The `Khan Academy`_ recommends using Google style.

The choice between styles is largely aesthetic, but the two styles should
not be mixed. Choose one style for your project and be consistent with it.


For full documentation see http://sphinxcontrib-napoleon.readthedocs.org



News 2020
===========


.. seealso::

   - :ref:`sphinx_2_4_0`
