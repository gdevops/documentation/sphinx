.. index::
   pair: sphinx ; applications

.. _sphinx_applications:

====================
sphinx applications
====================


.. toctree::
   :maxdepth: 4

   edit_on_web/edit_on_web
   on_github/on_github
   sphinx_wiki
   tinkerer/tinkerer
