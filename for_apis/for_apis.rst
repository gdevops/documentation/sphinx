

.. _sphinx_for_apis:

======================================================================
Sphinx for APIs (TOC entry for every function, class, and method)
======================================================================

.. toctree::
   :maxdepth: 3

   scanpy/scanpy
   autodocsum/autodocsum
   trojanzoo/trojanzoo
