

.. _trojanzoo:

======================================================================
TrojanZoo
======================================================================

- https://github.com/sphinx-doc/sphinx/issues/6316
- https://you.com/search?q=class%2Fmembers%20to%20show%20up%20in%20the%20TOC%20with%20sphinx%20%20autodoc&fromSearchBar=true

TrojanZoo
==========

- https://ain-soph.github.io/
- https://ain-soph.github.io/alpsplot/figure.html
- https://ain-soph.github.io/trojanzoo/trojanzoo/datasets.html#trojanzoo.datasets.Dataset
- https://github.com/ain-soph/trojanzoo_sphinx_theme


TrojanZoo provides a universal pytorch platform to conduct security
researches (especially backdoor attacks/defenses) of image classification
in deep learning.

It is composed of two packages: trojanzoo and trojanvision.

trojanzoo contains abstract classes and utilities, while trojanvision
contains abstract and concrete ones for image classification task.

