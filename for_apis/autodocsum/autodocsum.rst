.. index::
   ! autodocsum

.. _sphinx_autodocsum:

======================================================================
Sphinx for APIs autodocsumm
======================================================================

autodocsumm
=============

- https://you.com/search?q=class%2Fmembers%20to%20show%20up%20in%20the%20TOC%20with%20sphinx%20%20autodoc&fromSearchBar=true
- https://github.com/Chilipp/autodocsumm
