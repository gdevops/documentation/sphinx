.. index::
   pair: Sphinx ; Tooltip

.. _sphinx_hoverxref:

==============================================================================================================
**Sphinx-hoverxref** Tooltip with content embedded when hover an internal reference **(readthedocs only)**
==============================================================================================================

.. seealso::

   - https://x.com/reydelhumo
   - https://github.com/readthedocs/sphinx-hoverxref
   - https://sphinx-hoverxref.readthedocs.io/en/latest/
   - https://blog.readthedocs.com/czi-grant-announcement/
   - https://cpython-ericholscher.readthedocs.io/en/sphinx-hoverxref/whatsnew/3.9.html


Announce
==========

.. seealso::

   - https://blog.readthedocs.com/czi-grant-announcement/
   - https://x.com/readthedocs/status/1329506155404558336?s=20


Description
===========

**sphinx-hoverxref** is a Sphinx extension to show a floating window
(tooltips or modal dialogues) on the cross references of the documentation
embedding the content of the linked section on them. With sphinx-hoverxref,
you don’t need to click a link to see what’s in there.


Usage
=========

To show a floating window, use the role hoverxref to link to any document
or section and embed its content into it. We currently support two
different types of floating windows: Tooltip and Modal.


Demo
======

.. seealso::

   - https://sphinx-hoverxref.readthedocs.io/en/latest/
   - https://cpython-ericholscher.readthedocs.io/en/sphinx-hoverxref/whatsnew/3.9.html



``sphinx-hoverxref`` is a Sphinx_ extension to show a floating window
(*tooltips* or *modal* dialogues) on the cross references of the documentation
embedding the content of the linked section on them. With ``sphinx-hoverxref``,
you don't need to click a link to see what's in there.


.. _Sphinx: https://www.sphinx-doc.org/
.. _Read the Docs: https://readthedocs.org


.. |Build| image:: https://travis-ci.org/readthedocs/sphinx-hoverxref.svg?branch=master
   :target: https://travis-ci.org/readthedocs/sphinx-hoverxref
   :alt: Build status
.. |PyPI version| image:: https://img.shields.io/pypi/v/sphinx-hoverxref.svg
   :target: https://pypi.org/project/sphinx-hoverxref
   :alt: Current PyPI version
.. |Docs badge| image:: https://readthedocs.org/projects/sphinx-hoverxref/badge/?version=latest
   :target: https://sphinx-hoverxref.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation status
.. |License| image:: https://img.shields.io/github/license/readthedocs/sphinx-hoverxref.svg
   :target: LICENSE
   :alt: Repository license
