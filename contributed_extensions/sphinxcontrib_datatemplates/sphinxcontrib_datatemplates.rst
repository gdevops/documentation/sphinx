.. index::
   pair: sphinxcontrib-datatemplates ; extension
   pair: data ; sphinxcontrib-datatemplates
   ! sphinxcontrib-datatemplates
   ! datatemplates


.. _sphinxcontrib_datatemplates:

================================================================================
**sphinxcontrib-datatemplates**
================================================================================


- https://doughellmann.com/releases/sphinxcontrib-datatemplates-0-8-1/

Introduction
==============

sphinxcontrib.datatemplates is a Sphinx extension for rendering nicely
formatted HTML with parts of the output coming from JSON and YAML data files.

It is intended to be used to mix machine-readable data with prose.

Installation
==============

- https://sphinxcontribdatatemplates.readthedocs.io/en/latest/install.html

This project is available on PyPI, and can be installed using pip or poetry::


    pip install sphinxcontrib.datatemplates
    poetry add sphinxcontrib.datatemplates

You'll also want to add the extension to `extensions` in `conf.py`


.. code-block:: python

    extensions = [
        'sphinxcontrib.datatemplates',
    ]



