.. index::
   pair: vsketch; sphinx-api
   pair: Jinja; Template engine
   pair: Jinja; macros
   pair: macro jinja; macros.auto_summary
   ! Antoine Beyeler

.. _sphinx_antoine_beyeler:
.. _sphinx_antoine_beyeler_autoapi:

================================================================================================================================================
**Generating beautiful Python API documentation with Sphinx AutoAPI and Jinja (a template engine) by Antoine Beyeler**
================================================================================================================================================

.. figure:: images/twitter_announce.png
   :align: center

   https://x.com/abey79/status/1524067831154892801?s=20&t=o_xiUFXNuqK9KE3qOuKBlQ


- :ref:`vsketch`
- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/
- https://vsketch.readthedocs.io/en/latest/autoapi/vsketch/index.html
- https://github.com/readthedocs/sphinx-autoapi
- https://jinja.palletsprojects.com/en/3.1.x/
- https://github.com/sphinx-doc/sphinx/issues/6316
- https://you.com/search?q=class%2Fmembers%20to%20show%20up%20in%20the%20TOC%20with%20sphinx%20%20autodoc&fromSearchBar=true
- :ref:`sphinx_autoapi`


Specifications
================

- https://github.com/sphinx-doc/sphinx/issues/6316#issue-435904197


It would be useful to have an option that causes Sphinx to automatically
create a TOC entry for:

- **every function**,
- **every class**,
- **every method**.

In the absence of this, tables of contents are of limited value.


Antoine Beyeler
===============

- https://x.com/abey79/status/1521855335564300289?s=20&t=Pq7NAJLlVWgjuDX6FPFOmg

Antoine Beyeler, @abey79 2022-05-04 15h @mariatta @pradyunsg

Would it be possible to update this tutorial with the steps required for
the class/members to show up in the TOC with autodoc?
I haven't been able to achieve this to this day, though it's likely
something anyone would want.


I have yet to find a suitable workaround. autosummary is far from it
out-of-the-box.

Careful template crafting helps, but there is hardly any reference material
on those.

Generating beautiful Python API documentation with Sphinx AutoAPI and Jinja (a template engine)
============================================================================================================================

- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/
- https://vsketch.readthedocs.io/en/latest/autoapi/vsketch/index.html
- https://github.com/abey79/vsketch
- https://github.com/abey79/vsketch/pull/255
- https://github.com/abey79/vsketch/pull/255/files
- https://github.com/abey79/vsketch/tree/master/docs
- https://github.com/abey79/vsketch/blob/master/docs/conf.py
- https://github.com/readthedocs/sphinx-autoapi
- https://discord.com/channels/499297341472505858/748589023731122277/974060638416556056
- https://x.com/abey79/status/1524067831154892801?s=20&t=o_xiUFXNuqK9KE3qOuKBlQ


Despite #Sphinx ubiquity in the #Python world, it is surprisingly hard
to produce nice looking and functional API reference docs, with cross-ref'd
summaries and proper TOC support.

I spent days on this for #vsketch (https://github.com/abey79/vsketch/pull/255), a
nd wrote a tutorial with my findings:

.. figure:: images/twitter_announce.png
   :align: center

   https://x.com/abey79/status/1524067831154892801?s=20&t=o_xiUFXNuqK9KE3qOuKBlQ


.. figure:: images/discord_announce.png
   :align: center

   https://discord.com/channels/499297341472505858/748589023731122277/974060638416556056

.. figure:: images/vsketch.png
   :align: center

   https://vsketch.readthedocs.io/en/latest/autoapi/vsketch/index.html

.. figure:: images/intro.png
   :align: center

   https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/



Following a `recent discussion on Twitter <https://x.com/abey79/status/1521484512596746246?s=20&t=3kEAgQUDFiJDRzz77UdTgA>`_, I decided to take yet another
deep dive in my Python projects' documentation and fix once and for all
the issues I had with it.

.. figure:: images/how_it_started.png
   :align: center

I first focused on the automatically-generated API reference section and
this article details the results of my finding.

Specifically, I’m using `vsketch’s API reference <https://vsketch.readthedocs.io/en/latest/autoapi/vsketch/index.html>`_, which I recently updated,
as an example documentation source.

This article addresses the following objectives:

- **Produce a beautiful API documentation** based on the code docstrings
  that is both nice to look at and easy to navigate.
- Support for a proper table of content navigation down to each
  class/module’s member.
- **Nice looking summary tables** listing modules' and classes' contents.


Implementation
================

- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/
- https://github.com/abey79/vsketch
- https://github.com/abey79/vsketch/pull/255
- https://github.com/abey79/vsketch/pull/255/files
- https://vsketch.readthedocs.io/en/latest/index.html
- https://github.com/abey79/vsketch/tree/master/docs
- https://github.com/abey79/vsketch/blob/master/docs/conf.py
- https://github.com/readthedocs/sphinx-autoapi
- https://jinja.palletsprojects.com/en/3.1.x/




conf.py
=========

- https://github.com/abey79/vsketch/blob/master/docs/conf.py
- https://jinja.palletsprojects.com/en/3.1.x/api/#custom-tests

.. code-block:: python

    # Configuration file for the Sphinx documentation builder.
    #
    # This file only contains a selection of the most common options. For a full
    # list see the documentation:
    # https://www.sphinx-doc.org/en/master/usage/configuration.html


    # -- Project information -----------------------------------------------------

    project = "vsketch"
    copyright = "2020-2022, Antoine Beyeler"
    author = "Antoine Beyeler"

    # -- General configuration ---------------------------------------------------

    # Add any Sphinx extension module names here, as strings. They can be
    # extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
    # ones.
    extensions = [
        "sphinx.ext.intersphinx",
        "sphinx.ext.napoleon",
        "myst_parser",
        "sphinx_copybutton",
        "autoapi.extension",
    ]


    # Add any paths that contain templates here, relative to this directory.
    templates_path = ["_templates"]

    # List of patterns, relative to source directory, that match files and
    # directories to ignore when looking for source files.
    # This pattern also affects html_static_path and html_extra_path.
    exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "venv", ".*"]

    # -- Global options ----------------------------------------------------------

    # Don't mess with double-dash used in CLI options
    smartquotes_action = "qe"

    # -- Options for HTML output -------------------------------------------------

    # The theme to use for HTML and HTML Help pages.  See the documentation for
    # a list of builtin themes.
    #
    html_theme = "furo"

    # Add any paths that contain custom static files (such as style sheets) here,
    # relative to this directory. They are copied after the builtin static files,
    # so a file named "default.css" will overwrite the builtin "default.css".
    html_static_path = ["_static"]

    # -- Intersphinx options
    intersphinx_mapping = {
        "shapely": ("https://shapely.readthedocs.io/en/latest/", None),
        "vpype": ("https://vpype.readthedocs.io/en/latest/", None),
        "python": ("https://docs.python.org/3/", None),
        "numpy": ("https://numpy.org/doc/stable/", None),
    }

    # -- Napoleon options
    napoleon_include_init_with_doc = False

    # -- autoapi configuration ---------------------------------------------------

    autodoc_typehints = "signature"  # autoapi respects this

    autoapi_type = "python"
    autoapi_dirs = ["../vsketch"]
    autoapi_template_dir = "_templates/autoapi"
    autoapi_options = [
        "members",
        "undoc-members",
        "show-inheritance",
        "show-module-summary",
        "imported-members",
    ]
    # autoapi_python_use_implicit_namespaces = True
    autoapi_keep_files = True
    # autoapi_generate_api_docs = False


    # -- custom auto_summary() macro ---------------------------------------------


    def contains(seq, item):
        """Jinja custom test to check existence in a container.

        Example of use:
        {% set class_methods = methods|selectattr("properties", "contains", "classmethod") %}

        Related doc: https://jinja.palletsprojects.com/en/3.1.x/api/#custom-tests
        """
        return item in seq


    def prepare_jinja_env(jinja_env) -> None:
        """Add `contains` custom test to Jinja environment."""
        jinja_env.tests["contains"] = contains


    autoapi_prepare_jinja_env = prepare_jinja_env

    # Custom role for labels used in auto_summary() tables.
    rst_prolog = """
    .. role:: summarylabel
    """

    # Related custom CSS
    html_css_files = [
        "css/label.css",
    ]


    # noinspection PyUnusedLocal
    def autoapi_skip_members(app, what, name, obj, skip, options):
        """
            set_what={'method', 'package', 'function', 'class', 'data', 'exception', 'attribute', 'module'}
        """

        # skip submodules
        if what == "module":
            skip = True
        elif what == "data":
            if obj.name in ["EASING_FUNCTIONS", "ParamType"]:
                skip = True
        elif what == "function":
            if obj.name in ["working_directory"]:
                skip = True
        elif "vsketch.SketchClass" in name:
            if obj.name in [
                "vsk",
                "param_set",
                "execute_draw",
                "ensure_finalized",
                "execute",
                "get_params",
                "set_param_set",
            ]:
                skip = True
        elif "vsketch.Param" in name:
            if obj.name in ["set_value", "set_value_with_validation"]:
                skip = True
        return skip


    def setup(app):
        app.connect("autoapi-skip-member", autoapi_skip_members)


skip members, python mmapper
===============================

- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/
- https://github.com/readthedocs/sphinx-autoapi/blob/master/autoapi/mappers/python/mapper.py

.. code-block:: python

    set_what = set()
    def autoapi_skip_members(app, what, name, obj, skip, options):
        """
        """
        skip = False
        if name in [
            "intranet.conftest",
            "intranet.manage",
            "intranet.manage_dev",
            "intranet.manage_prod",
        ]:
            skip = True
            # print(f"autoapi_skip_members() {what=} {name=}")

        if "intranet.sorl" in name:
            skip = True

        set_what.add(what)
        return skip


    def setup(app):
        app.connect("autoapi-skip-member", autoapi_skip_members)

    def print_etat_objects():
        print(f"{set_what=}")

    import atexit

    atexit.register(print_etat_objects)


Results
----------

::

    set_what={'method', 'package', 'function', 'class', 'data', 'exception', 'attribute', 'module'}


- https://github.com/readthedocs/sphinx-autoapi/blob/master/autoapi/mappers/python/__init__.py
- https://raw.githubusercontent.com/readthedocs/sphinx-autoapi/master/autoapi/mappers/python/__init__.py


AutoAPI objects
===================

- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/#autoapi-objects
- https://github.com/readthedocs/sphinx-autoapi
- https://github.com/readthedocs/sphinx-autoapi/graphs/contributors
- https://sphinx-autoapi.readthedocs.io/en/latest/reference/config.html#confval-autoapi_options


**Understanding the Sphinx AutoAPI objects is key to customising jinja templates**.
They are one of the major difference with respect to autodoc/autosummary.

In order to generate the API documentation, the autodoc loads your actual
code and uses Python’s introspection capabilities to extract the required
information from your module and class objects.

In contrast, **Sphinx AutoAPI parses your Python code** and builds a collection
of so-called **mapper** objects which describe your code and its structure.

These objects are then passed on as context to the :ref:`Jinja templating engine <autoapi_jinja_templates>`.

**Oddly, the documentation doesn’t provide a reference about them**, but their
implementation is easy to read.



readthedocs/sphinx-autoapi/master/autoapi/mappers/python/__init__.py
------------------------------------------------------------------------

.. code-block:: python
   :linenos:

    from .mapper import PythonSphinxMapper
    from .objects import (
        PythonClass,
        PythonFunction,
        PythonModule,
        PythonMethod,
        PythonPackage,
        PythonAttribute,
        PythonData,
        PythonException,
    )

readthedocs/sphinx-autoapi/master/autoapi/mappers/python/mapper.py
-----------------------------------------------------------------------

- https://raw.githubusercontent.com/readthedocs/sphinx-autoapi/master/autoapi/mappers/python/mapper.py

.. code-block:: python
   :linenos:

    class PythonSphinxMapper(SphinxMapperBase):

        """Auto API domain handler for Python

        Parses directly from Python files.

        :param app: Sphinx application passed in as part of the extension
        """

        _OBJ_MAP = {
            cls.type: cls
            for cls in (
                PythonClass,
                PythonFunction,
                PythonModule,
                PythonMethod,
                PythonPackage,
                PythonAttribute,
                PythonData,
                PythonException,
            )
        }


.. _autoapi_jinja_templates:

Autoapi Jinja templates (vsketch/tree/master/docs/_templates/autoapi)
============================================================================

- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/
- https://jinja.palletsprojects.com/en/3.1.x/
- https://github.com/pallets/jinja
- https://x.com/PalletsTeam
- https://x.com/davidism
- https://github.com/abey79/vsketch/tree/master/docs/_templates/autoapi

Description
--------------

Jinja is a fast, expressive, extensible templating engine.
Special placeholders in the template allow writing code similar to Python
syntax. Then the template is passed data to render the final document.

It includes:

- Template inheritance and inclusion.
- Define and import `macros <macros_vsketch>` within templates.
- HTML templates can use autoescaping to prevent XSS from untrusted user input.
- A sandboxed environment can safely render untrusted templates.
- AsyncIO support for generating templates and calling async functions.
- I18N support with Babel.
- Templates are compiled to optimized Python code just-in-time and cached,
  or can be compiled ahead-of-time.
- Exceptions point to the correct line in templates to make debugging easier.
- Extensible filters, tests, functions, and even syntax.-

Jinja's philosophy is that while application logic belongs in Python if
possible, it shouldn't make the template designer's job difficult by
restricting functionality too much.


**Autoapi jinja Python templates**
-----------------------------------------

- https://github.com/readthedocs/sphinx-autoapi/tree/master/autoapi/templates/python

::

    sphinx-autoapi/autoapi/templates
        └── python
            ├── attribute.rst
            ├── class.rst
            ├── data.rst
            ├── exception.rst
            ├── function.rst
            ├── method.rst
            ├── module.rst
            └── package.rst


vsketch jinja vsketch/docs/_templates
-----------------------------------------

- https://github.com/abey79/vsketch/blob/master/docs/_templates

Redefinition of Autoapi **class** and **module**.

::

   vsketch/docs/_templates

    └── autoapi
        ├── index.rst
        ├── macros.rst
        └── python
            ├── class.rst
            └── module.rst


vsketch jinja  _template.autoapi.index.rst
-----------------------------------------------

- https://github.com/abey79/vsketch/blob/master/docs/_templates/autoapi/index.rst


.. code-block:: jinja
   :linenos:

    API Reference
    =============

    This page contains auto-generated API reference documentation.

    .. toctree::
       :titlesonly:

       {% for page in pages %}
       {% if page.top_level_object and page.display %}
       {{ page.include_path }}
       {% endif %}
       {% endfor %}



.. _macros_vsketch:

vsketch jinja _template.autoapi.macros.rst (Macro Jinja **macros.auto_summary**)
-------------------------------------------------------------------------------------

- https://github.com/abey79/vsketch/blob/master/docs/_templates/autoapi/macros.rst
- https://jinja.palletsprojects.com/en/3.1.x/templates/#macros
- https://bylr.info/articles/2022/05/10/api-doc-with-sphinx-autoapi/#basic-macro-setup

.. code-block:: jinja
   :linenos:


    {# AutoApiSummary replacement macro #}
    {#
    The intent of this macro is to replace the autoapisummary directive with the following
    improvements:

    1) Method signature is generated without typing annotation regardless of the value of
       `autodoc_typehints`
    2) Properties are treated like attribute (but labelled as properties).
    3) Label are added as summary prefix to indicate if the member is a property, class
       method, or static method.

    Copyright: Antoine Beyeler, 2022
    License: MIT
    #}

    {# Renders an object's name with a proper reference and optional signature.

    The signature is re-generated from obj.obj.args, which is undocumented but is the
    only way to have a type-less signature if `autodoc_typehints` is `signature` or
    `both`. #}
    {% macro _render_item_name(obj, sig=False) -%}
    :py:obj:`{{ obj.name }} <{{ obj.id }}>`
         {%- if sig -%}
           \ (
           {%- for arg in obj.obj.args -%}
              {%- if arg[0] %}{{ arg[0]|replace('*', '\*') }}{% endif -%}{{  arg[1] -}}
              {%- if not loop.last  %}, {% endif -%}
           {%- endfor -%}
           ){%- endif -%}
    {%- endmacro %}


    {# Generates a single object optionally with a signature and a labe. #}
    {% macro _item(obj, sig=False, label='') %}
       * - {{ _render_item_name(obj, sig) }}
         - {% if label %}:summarylabel:`{{ label }}` {% endif %}{% if obj.summary %}{{ obj.summary }}{% else %}\-{% endif +%}
    {% endmacro %}



    {# Generate an autosummary-like table with the provided members. #}
    {% macro auto_summary(objs, title='') -%}

    .. list-table:: {{ title }}
       :header-rows: 0
       :widths: auto
       :class: summarytable {#- apply CSS class to customize styling +#}

      {% for obj in objs -%}
        {#- should the full signature be used? -#}
        {%- set sig = (obj.type in ['method', 'function'] and not 'property' in obj.properties) -%}

        {#- compute label -#}
        {%- if 'property' in obj.properties -%}
          {%- set label = 'prop' -%}
        {%- elif 'classmethod' in obj.properties -%}
          {%- set label = 'class' -%}
        {%- elif 'abstractmethod' in obj.properties -%}
          {%- set label = 'abc' -%}
        {%- elif 'staticmethod' in obj.properties -%}
          {%- set label = 'static' -%}
        {%- else -%}
          {%- set label = '' -%}
        {%- endif -%}

        {{- _item(obj, sig=sig, label=label) -}}
      {%- endfor -%}

    {% endmacro %}


vsketch jinja  _template.autoapi.python.class.rst
------------------------------------------------------

- https://github.com/abey79/vsketch/blob/master/docs/_templates/autoapi/python/class.rst
- https://github.com/readthedocs/sphinx-autoapi/blob/master/autoapi/templates/python/class.rst

.. code-block:: jinja
   :linenos:

    {% import 'macros.rst' as macros %}

    {% if obj.display %}
    .. py:{{ obj.type }}:: {{ obj.short_name }}{% if obj.args %}({{ obj.args }}){% endif %}
    {% for (args, return_annotation) in obj.overloads %}
       {{ " " * (obj.type | length) }}   {{ obj.short_name }}{% if args %}({{ args }}){% endif %}
    {% endfor %}


       {% if obj.bases %}
       {% if "show-inheritance" in autoapi_options %}
       Bases: {% for base in obj.bases %}{{ base|link_objs }}{% if not loop.last %}, {% endif %}{% endfor %}
       {% endif %}


       {% if "show-inheritance-diagram" in autoapi_options and obj.bases != ["object"] %}
       .. autoapi-inheritance-diagram:: {{ obj.obj["full_name"] }}
          :parts: 1
          {% if "private-members" in autoapi_options %}
          :private-bases:
          {% endif %}

       {% endif %}
       {% endif %}
       {% if obj.docstring %}
       {{ obj.docstring|indent(3) }}
       {% endif %}
       {% if "inherited-members" in autoapi_options %}
       {% set visible_classes = obj.classes|selectattr("display")|list %}
       {% else %}
       {% set visible_classes = obj.classes|rejectattr("inherited")|selectattr("display")|list %}
       {% endif %}
       {% for klass in visible_classes %}
       {{ klass.render()|indent(3) }}
       {% endfor %}
       {% if "inherited-members" in autoapi_options %}
       {% set visible_attributes = obj.attributes|selectattr("display")|list %}
       {% else %}
       {% set visible_attributes = obj.attributes|rejectattr("inherited")|selectattr("display")|list %}
       {% endif %}
       {% if "inherited-members" in autoapi_options %}
       {% set visible_methods = obj.methods|selectattr("display")|list %}
       {% else %}
       {% set visible_methods = obj.methods|rejectattr("inherited")|selectattr("display")|list %}
       {% endif %}

       {% if visible_methods or visible_attributes %}
       .. rubric:: Overview

       {% set summary_methods = visible_methods|rejectattr("properties", "contains", "property")|list %}
       {% set summary_attributes = visible_attributes + visible_methods|selectattr("properties", "contains", "property")|list %}
       {% if summary_attributes %}
       {{ macros.auto_summary(summary_attributes, title="Attributes")|indent(3) }}
       {% endif %}

       {% if summary_methods %}
       {{ macros.auto_summary(summary_methods, title="Methods")|indent(3) }}
       {% endif %}

       .. rubric:: Members

       {% for attribute in visible_attributes %}
       {{ attribute.render()|indent(3) }}
       {% endfor %}
       {% for method in visible_methods %}
       {{ method.render()|indent(3) }}
       {% endfor %}
       {% endif %}
    {% endif %}



vsketch jinja  _template.autoapi.python.module.rst
--------------------------------------------------------

.. code-block:: jinja
   :linenos:


    {% import 'macros.rst' as macros %}

    {% if not obj.display %}
    :orphan:

    {% endif %}
    {{ obj.name }}
    {{ "=" * obj.name|length }}

    .. py:module:: {{ obj.name }}

    {% if obj.docstring %}
    .. autoapi-nested-parse::

       {{ obj.docstring|indent(3) }}

    {% endif %}

    {% block subpackages %}
    {% set visible_subpackages = obj.subpackages|selectattr("display")|list %}
    {% if visible_subpackages %}
    Subpackages
    -----------
    .. toctree::
       :titlesonly:
       :maxdepth: 3

    {% for subpackage in visible_subpackages %}
       {{ subpackage.short_name }}/index.rst
    {% endfor %}


    {% endif %}
    {% endblock %}
    {% block submodules %}
    {% set visible_submodules = obj.submodules|selectattr("display")|list %}
    {% if visible_submodules %}
    Submodules
    ----------
    .. toctree::
       :titlesonly:
       :maxdepth: 1

    {% for submodule in visible_submodules %}
       {{ submodule.short_name }}/index.rst
    {% endfor %}


    {% endif %}
    {% endblock %}
    {% block content %}
    {% if obj.all is not none %}
    {% set visible_children = obj.children|selectattr("display")|selectattr("short_name", "in", obj.all)|list %}
    {% elif obj.type is equalto("package") %}
    {% set visible_children = obj.children|selectattr("display")|list %}
    {% else %}
    {% set visible_children = obj.children|selectattr("display")|rejectattr("imported")|list %}
    {% endif %}
    {% if visible_children %}
    Overview
    --------

    {% set visible_classes = visible_children|selectattr("type", "equalto", "class")|list %}
    {% set visible_functions = visible_children|selectattr("type", "equalto", "function")|list %}
    {% set visible_attributes = visible_children|selectattr("type", "equalto", "data")|list %}
    {% if "show-module-summary" in autoapi_options and (visible_classes or visible_functions) %}
    {% block classes scoped %}
    {% if visible_classes %}
    {{ macros.auto_summary(visible_classes, title="Classes") }}
    {% endif %}
    {% endblock %}

    {% block functions scoped %}
    {% if visible_functions %}
    {{ macros.auto_summary(visible_functions, title="Function") }}
    {% endif %}
    {% endblock %}

    {% block attributes scoped %}
    {% if visible_attributes %}
    {{ macros.auto_summary(visible_attributes, title="Attributes") }}
    {% endif %}
    {% endblock %}
    {% endif %}

    {% if visible_classes %}
    Classes
    -------
    {% for obj_item in visible_classes %}
    {{ obj_item.render()|indent(0) }}
    {% endfor %}
    {% endif %}

    {% if visible_functions %}
    Functions
    ---------
    {% for obj_item in visible_functions %}
    {{ obj_item.render()|indent(0) }}
    {% endfor %}
    {% endif %}

    {% if visible_attributes %}
    Attributes
    ----------
    {% for obj_item in visible_attributes %}
    {{ obj_item.render()|indent(0) }}
    {% endfor %}
    {% endif %}


    {% endif %}
    {% endblock %}



vsketch CSS
=============

- https://github.com/abey79/vsketch/blob/master/docs/_static/css/label.css

_static.css.label.css
------------------------

.. code-block:: css

    .summarylabel {
        background-color: var(--color-foreground-secondary);
        color: var(--color-background-secondary);
        font-size: 70%;
        padding-left: 2px;
        padding-right: 2px;
        border-radius: 3px;
        vertical-align: 15%;
        padding-bottom: 2px;
        filter: opacity(40%);
    }


    table.summarytable {
        width: 100%;
    }

