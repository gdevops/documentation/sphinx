.. index::
   pair: Sphinx ; comments

.. _sphinx_comments:

========================================================================
**Sphinx comments** **hypothes.is** interaction layer with Sphinx
========================================================================

.. seealso::

   - https://github.com/executablebooks/sphinx-comments
   - https://x.com/choldgraf
   - https://github.com/executablebooks/sphinx-comments/graphs/contributors
   - https://sphinx-comments.readthedocs.io/en/latest/
   - https://web.hypothes.is/
   - https://x.com/hypothes_is
   - https://web.hypothes.is/about/
   - https://github.com/hypothesis
   - https://github.com/hypothesis/h



Add comments and annotation functionality to your Sphinx website
===================================================================

Currently, these commenting engines are supported:

- Hypothes.is provides a web overlay that allows you to annotate
  and comment collaboratively.
- utteranc.es is a web commenting system that uses GitHub Issues to
  store and manage comments.
- dokie.li is an open source commenting and annotation overlay built
  on web standards.


Example
----------

.. seealso::

   - https://sphinx-comments.readthedocs.io/en/latest/hypothesis.html#activate-hypothes-is
   - https://hypothes.is/users/pvergain
   - https://x.com/pvergain/status/1309383190801457152?s=20


Repository structure
=======================

Sphinx Comments is a lightweight Sphinx extension that activates several
Javascript libraries for use within Sphinx.

All of its functionality is contained in sphinx_comments/__init__.py.

As a general rule, Sphinx Comments tries to be as lightweight as possible.

It simply:

- Loads Javscript libraries for web commenting and annotation platforms
- Provides a configuration layer for platforms that support it

Note that some of these platforms cannot be activated at the same time,
users will need to choose one or the other.

Some of the annotation platforms require more complex setup - for example,
utteranc.es requires its script to be placed in a specific location on
the page, and so sphinx-comments will place it directly in the doctree
of the page (underneath the content).

