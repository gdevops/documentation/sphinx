.. index::
   pair: Sphinx ; multiversion
   ! sphinx-multiversion

.. _ref_sphinx_multiversion:

============================================
**sphinx-multiversion**
============================================

.. seealso::

   - https://github.com/Holzhaus/sphinx-multiversion
   - https://holzhaus.github.io/sphinx-multiversion/master/index.html



Introduction
============



Quick start
==============

.. toctree::
   :maxdepth: 3

   quick_start/quick_start.rst

Article
=========

.. seealso::

   - https://blog.stephane-robert.info/post/sphinx-documentation-multi-version/
