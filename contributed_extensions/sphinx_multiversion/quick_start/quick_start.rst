
.. _sphinx_multiversion_quickstart:

============================================
Quick start
============================================

.. seealso::

   - https://holzhaus.github.io/sphinx-multiversion/master/quickstart.html
   - https://holzhaus.github.io/sphinx-multiversion/master/configuration.html#configuration
   - https://holzhaus.github.io/sphinx-multiversion/master/templates.html#templates




Introduction
===============

After installation, using sphinx-multiversion should be fairly straightforward.

To be able to build multiple versions of Sphinx documentation, sphinx-multiversion
acts as wrapper for sphinx-build.

If you’re already using Sphinx documentation for your project, you can
now use sphinx-multiversion to build the HTML documentation.

You can check if it works by running::

    # Without sphinx-multiversion
    sphinx-build docs build/html

    # With sphinx-multiversion
    sphinx-multiversion docs build/html

Don’t worry - no version picker will show up in the generated HTML yet.
You need to `configure <https://holzhaus.github.io/sphinx-multiversion/master/configuration.html#configuration>`_ the extension first.


If you’re not using Sphinx yet, have a look at the tutorial.

Next, you need to add the extension to the conf.py file::

    extensions = [
        "sphinx_multiversion",
    ]


You can also list branches, tags, released versions and development
branches separately. See `Templates <https://holzhaus.github.io/sphinx-multiversion/master/templates.html#templates>`_ for details.

Assuming that you’re using a theme with sidebar widget support, you just
need to make sure that the file is inside the templates_path and add
it to the html_sidebars variable::

    templates_path = [
        "_templates",
    ]

    html_sidebars = {
        '**': [
            'versioning.html',
        ],
    }

Now rebuild the documentation::

    sphinx-multiversion docs build/html

Done!

To make the different versions show up in the HTML, you also need to
add a custom template.

For example, you could create a new template named versioning.html with
the following content::

    {% if versions %}
    <h3>{{ _('Versions') }}</h3>
    <ul>
      {%- for item in versions %}
      <li><a href="{{ item.url }}">{{ item.name }}</a></li>
      {%- endfor %}
    </ul>
    {% endif %}



