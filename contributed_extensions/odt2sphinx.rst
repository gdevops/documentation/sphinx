
.. index::
   sphinx extension (plantuml)

.. _odt2sphinx:

===========================
sphinx odt2sphinx extension
===========================

.. seealso::

   - https://bitbucket.org/cdevienne/odt2sphinx


Transform a OOo Writer document into Sphinx documentation sources
