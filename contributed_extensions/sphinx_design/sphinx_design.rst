.. index::
   pair: sphinx-design ; extension
   pair: Sphinx ; design
   ! sphinx-design


.. _sphinx_design:

==============================================================================================================
|eb|  **sphinx-design A sphinx extension for designing beautiful, screen-size responsive web components**
==============================================================================================================

- https://github.com/executablebooks/sphinx-design
- https://sphinx-design.readthedocs.io/en/furo-theme/
- https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
- https://github.com/chrisjsewell


Referenced by
================

- https://pradyunsg.me/furo/reference/tabs/#sphinx-design


Migrating from sphinx-panels
==================================

This package arose as an iteration on sphinx-panels, with the intention
to make it more flexible, easier to use, and minimise CSS clashes wth
sphinx themes.

Notable changes
=================

Reduce direct use of CSS classes

These are replaced by the use of directive options, which are:

- Easier to understand
- Easier to validate
- Easier to work with non-HTML outputs
- Easier to improve/refactor


Improved CSS
===============

Updated Bootstrap CSS from v4 -> v5, which in particular allows top-level
grid to define both column numbers and gutter sizes.

All CSS classes are prefixed with sd- (no clash with other theme/extension CSS)

All colors use CSS variables (customisable)


widgets
==========

::


    ::::{grid} 1 2 2 3
    :margin: 4 4 0 0
    :gutter: 1

    :::{grid-item-card} {octicon}`table` Grids
    :link: grids
    :link-type: doc

    Screen size adaptable grid layouts.
    :::

    :::{grid-item-card} {octicon}`note` Cards
    :link: cards
    :link-type: doc

    Flexible and extensible content containers.
    :::

    :::{grid-item-card} {octicon}`chevron-down` Dropdowns
    :link: dropdowns
    :link-type: doc

    Hide content in expandable containers.
    :::

    :::{grid-item-card} {octicon}`duplicate` Tabs
    :link: tabs
    :link-type: doc

    Synchronisable, tabbed content sets.
    :::

    :::{grid-item-card} {octicon}`plus-circle` Badges, Buttons & Icons
    :link: badges_buttons
    :link-type: doc

    Roles and directives for {bdg-primary}`badges` and other components.
    :::

    :::{grid-item-card} {octicon}`image` CSS Styling
    :link: css_variables
    :link-type: doc

    Change the default colors and other CSS.
    :::



.. toctree::
   :maxdepth: 3

   article/article
   badges_buttons/badges_buttons
   cards/cards
   div/div
   dropdowns/dropdowns
   grids/grids
   icon/icon
   tabs/tabs
   versions/versions

