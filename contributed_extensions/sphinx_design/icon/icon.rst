
===================
icon
===================


icon-fontawsome
===================

FontAwesome icons are added via the Fontawesome CSS classes.
If the theme you are using does not already include the FontAwesome CSS,
it should be loaded in your configuration from a `font-awesome CDN <https://cdnjs.com/libraries/font-awesome>`_,
with the `html_css_files <https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-html_css_files>`_

option, e.g.::

   html_css_files = ["https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css"]

::

    An icon :fa:`spinner;sd-bg-primary sd-bg-text-primary`, some more text.


An icon :fa:`spinner;sd-bg-primary sd-bg-text-primary`, some more text.


icon-material-design
======================

::

    - A regular icon: :material-regular:`data_exploration;2em`, some more text
    - A coloured regular icon: :material-regular:`settings;3em;sd-text-success`, some more text.
    - A coloured outline icon: :material-outlined:`settings;3em;sd-text-success`, some more text.
    - A coloured sharp icon: :material-sharp:`settings;3em;sd-text-success`, some more text.
    - A coloured round icon: :material-round:`settings;3em;sd-text-success`, some more text.
    - A coloured two-tone icon: :material-twotone:`settings;3em;sd-text-success`, some more text.
    - A fixed size icon: :material-regular:`data_exploration;24px`, some more text.


- A regular icon: :material-regular:`data_exploration;2em`, some more text
- A coloured regular icon: :material-regular:`settings;3em;sd-text-success`, some more text.
- A coloured outline icon: :material-outlined:`settings;3em;sd-text-success`, some more text.
- A coloured sharp icon: :material-sharp:`settings;3em;sd-text-success`, some more text.
- A coloured round icon: :material-round:`settings;3em;sd-text-success`, some more text.
- A coloured two-tone icon: :material-twotone:`settings;3em;sd-text-success`, some more text.
- A fixed size icon: :material-regular:`data_exploration;24px`, some more text.


icon-octicon
===============

- https://primer.style/octicons/
- https://github.com/google/material-design-icons
- https://fontawesome.com/icons?d=gallery&m=free

Inline icon roles are available for the GitHub octicon, Google Material
Design Icons, or FontAwesome libraries.

Octicon icons and Material icons are added as SVG’s directly into the
page with the octicon and material-<flavor> roles.
See below for the different flavors of Material Design Icons.

By default the icon will be of height 1em (i.e. the height of the font).
A specific height can be set after a semi-colon (;) with units of
either px, em or rem.

Additional CSS classes can also be added to the SVG after a second
semi-colon (;) delimiter.



::

    A coloured icon: :octicon:`report;1em;sd-text-info`, some more text.


A coloured icon: :octicon:`report;1em;sd-text-info`, some more text.
