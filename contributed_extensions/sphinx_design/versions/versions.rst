.. index::
   pair: sphinx-design ; versions

.. _sphinx_design_versions:

======================================================================================================
**versions**
======================================================================================================

- https://github.com/executablebooks/sphinx-design/releases

.. figure:: images/github_contributors.png
   :align: center

   https://github.com/executablebooks/sphinx-design/graphs/contributors


.. toctree::
   :maxdepth: 3

   0.4.1/0.4.1
   0.4.0/0.4.0

