
==============
dropdown
==============

dropdown basic
==============

::

    .. dropdown::

        Dropdown content

    .. dropdown:: Dropdown title

        Dropdown content

    .. dropdown:: Open dropdown
        :open:

        Dropdown content

.. dropdown::

    Dropdown content

.. dropdown:: Dropdown title

    Dropdown content

.. dropdown:: Open dropdown
    :open:

    Dropdown content

