
=============
Grids
=============

- https://sphinx-design.readthedocs.io/en/furo-theme/grids.html

Introduction
=================

Grids are based on a 12 column system, which can adapt to the size of
the viewing screen.

A grid directive can be set with the number of default columns (1 to 12);
either a single number for all screen sizes, or four numbers for extra-small
(<576px), small (768px), medium (992px) and large screens (>1200px),
then child grid-item directives should be set for each item.

Try re-sizing the screen to see the number of columns change:

grid basic
=============

::

    .. grid:: 1 2 3 4
        :outline:

        .. grid-item::

            A

        .. grid-item::

            B

        .. grid-item::

            C

        .. grid-item::

            D

.. grid:: 1 2 3 4
    :outline:

    .. grid-item::

        A

    .. grid-item::

        B

    .. grid-item::

        C

    .. grid-item::

        D


grid-card
============

::

    .. grid:: 2

        .. grid-item-card::  Title 1

            A

        .. grid-item-card::  Title 2

            B

.. grid:: 2

    .. grid-item-card::  Title 1

        A

    .. grid-item-card::  Title 2

        B


grid-card-columns
=====================

::

    .. grid:: 2

        .. grid-item-card::
            :columns: auto

            A

        .. grid-item-card::
            :columns: 12 6 6 6

            B

        .. grid-item-card::
            :columns: 12

            C


.. grid:: 2

    .. grid-item-card::
        :columns: auto

        A

    .. grid-item-card::
        :columns: 12 6 6 6

        B

    .. grid-item-card::
        :columns: 12

        C


grid-gutter
===============

::

    .. grid:: 2
        :gutter: 1

        .. grid-item-card::

            A

        .. grid-item-card::

            B

    .. grid:: 2
        :gutter: 3 3 4 5

        .. grid-item-card::

            A

        .. grid-item-card::

        B

.. grid:: 2
    :gutter: 1

    .. grid-item-card::

        A

    .. grid-item-card::

        B

.. grid:: 2
    :gutter: 3 3 4 5

    .. grid-item-card::

        A

    .. grid-item-card::

        B


grid-nested
============

::

    .. grid:: 1 1 2 2
        :gutter: 1

        .. grid-item::

            .. grid:: 1 1 1 1
                :gutter: 1

                .. grid-item-card:: Item 1.1

                    Multi-line

                    content

                .. grid-item-card:: Item 1.2

                    Content

        .. grid-item::

            .. grid:: 1 1 1 1
                :gutter: 1

                .. grid-item-card:: Item 2.1

                    Content

                .. grid-item-card:: Item 2.2

                    Content

                .. grid-item-card:: Item 2.3

                    Content



.. grid:: 1 1 2 2
    :gutter: 1

    .. grid-item::

        .. grid:: 1 1 1 1
            :gutter: 1

            .. grid-item-card:: Item 1.1

                Multi-line

                content

            .. grid-item-card:: Item 1.2

                Content

    .. grid-item::

        .. grid:: 1 1 1 1
            :gutter: 1

            .. grid-item-card:: Item 2.1

                Content

            .. grid-item-card:: Item 2.2

                Content

            .. grid-item-card:: Item 2.3

                Content

