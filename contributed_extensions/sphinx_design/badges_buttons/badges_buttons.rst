
===============
Badges-button
===============



Badges basic
==============

::

    :bdg:`plain badge`

    :bdg-primary:`primary`, :bdg-primary-line:`primary-line`

    :bdg-secondary:`secondary`, :bdg-secondary-line:`secondary-line`

    :bdg-success:`success`, :bdg-success-line:`success-line`

    :bdg-info:`info`, :bdg-info-line:`info-line`

    :bdg-warning:`warning`, :bdg-warning-line:`warning-line`

    :bdg-danger:`danger`, :bdg-danger-line:`danger-line`

    :bdg-light:`light`, :bdg-light-line:`light-line`

    :bdg-dark:`dark`, :bdg-dark-line:`dark-line`


:bdg:`plain badge`

:bdg-primary:`primary`, :bdg-primary-line:`primary-line`

:bdg-secondary:`secondary`, :bdg-secondary-line:`secondary-line`

:bdg-success:`success`, :bdg-success-line:`success-line`

:bdg-info:`info`, :bdg-info-line:`info-line`

:bdg-warning:`warning`, :bdg-warning-line:`warning-line`

:bdg-danger:`danger`, :bdg-danger-line:`danger-line`

:bdg-light:`light`, :bdg-light-line:`light-line`

:bdg-dark:`dark`, :bdg-dark-line:`dark-line`



Button link
=============

::

    .. button-link:: https://example.com

    .. button-link:: https://example.com

        Button text

    .. button-link:: https://example.com
        :color: primary
        :shadow:

    .. button-link:: https://example.com
        :color: primary
        :outline:

    .. button-link:: https://example.com
        :color: secondary
        :expand:


.. button-link:: https://example.com

.. button-link:: https://example.com

    Button text

.. button-link:: https://example.com
    :color: primary
    :shadow:

.. button-link:: https://example.com
    :color: primary
    :outline:

.. button-link:: https://example.com
    :color: secondary
    :expand:

