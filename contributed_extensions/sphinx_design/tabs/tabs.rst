
===========
tab
===========


tab-basic
===========

::

    .. tab-set::

        .. tab-item:: Label1

            Content 1

        .. tab-item:: Label2

            Content 2


.. tab-set::

    .. tab-item:: Label1

        Content 1

    .. tab-item:: Label2

        Content 2


tab-set-code
============

::

    .. tab-set-code::

        .. literalinclude:: snippet.py
            :language: python

        .. code-block:: javascript

            a = 1;

.. tab-set-code::

    .. literalinclude:: ./snippet.py
        :language: python

    .. code-block:: javascript

        a = 1;


tab-set
=========

::

    .. tab-set::

        .. tab-item:: Label1.1
            :sync: key1

            Content 1

        .. tab-item:: Label2.1
            :sync: key2

            Content 2

    .. tab-set::

        .. tab-item:: Label1.2
            :sync: key1

            Content 1

        .. tab-item:: Label2.2
            :sync: key2

            Content 2

.. tab-set::

    .. tab-item:: Label1
        :sync: key1

        Content 1

    .. tab-item:: Label2
        :sync: key2

        Content 2

.. tab-set::

    .. tab-item:: Label1
        :sync: key1

        Content 1

    .. tab-item:: Label2
        :sync: key2

        Content 2


