"""Gestion des tiers."""
import enum
import logging
import operator
import time
from typing import Any

from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _
from tools.util_enum import choiceadapter
