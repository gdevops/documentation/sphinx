============
Cards
============


card basic
============

::

    .. card:: Card Title

        Card content


.. card:: Card Title

    Card content


card carroussel
=================

::

    .. card-carousel:: 2

        .. card:: card 1

            content

        .. card:: card 2

            Longer

            content

        .. card:: card 3

        .. card:: card 4

        .. card:: card 5

        .. card:: card 6


.. card-carousel:: 2

    .. card:: card 1

        content

    .. card:: card 2

        Longer

        content

    .. card:: card 3

    .. card:: card 4

    .. card:: card 5

    .. card:: card 6



::

    .. card:: Card Title

        Header
        ^^^
        Card content
        +++
        Footer


.. card:: Card Title

    Header
    ^^^
    Card content
    +++
    Footer

card images
============

::

    .. grid:: 2 3 3 4

        .. grid-item::

            .. card:: Title
                :img-background: ../images/particle_background.jpg
                :class-card: sd-text-black

                Text

        .. grid-item-card:: Title
            :img-top: ../images/particle_background.jpg

            Header
            ^^^
            Content
            +++
            Footer

        .. grid-item-card:: Title
            :img-bottom: ../images/particle_background.jpg

            Header
            ^^^
            Content
            +++
            Footer


::


    .. grid:: 2 3 3 4

        .. grid-item::

            .. card:: Title
                :img-background: ../images/particle_background.jpg
                :class-card: sd-text-black

                Text

        .. grid-item-card:: Title
            :img-top: ../images/particle_background.jpg

            Header
            ^^^
            Content
            +++
            Footer

        .. grid-item-card:: Title
            :img-bottom: ../images/particle_background.jpg

            Header
            ^^^
            Content
            +++
            Footer


Card link
=========


::


    .. _cards-clickable:

    Cards Clickable
    ...............

    .. card:: Clickable Card (external)
        :link: https://example.com

        The entire card can be clicked to navigate to https://example.com.

    .. card:: Clickable Card (internal)
        :link: cards-clickable
        :link-type: ref

        The entire card can be clicked to navigate to the ``cards`` reference target.



.. _cards-clickable:

Cards Clickable
...............

.. card:: Clickable Card (external)
    :link: https://example.com

    The entire card can be clicked to navigate to https://example.com.

.. card:: Clickable Card (internal)
    :link: cards-clickable
    :link-type: ref

    The entire card can be clicked to navigate to the ``cards`` reference target.


card-title link
===============

::

    .. _target:
    .. card:: Card Title https://example.com :ref:`link <target>`

        Card content


.. _target:
.. card:: Card Title https://example.com :ref:`link <target>`

    Card content



