
.. _sphinx_gallery_versions:

=======================================================================================
**sphinx-gallery** versions
=======================================================================================

.. seealso::

   - https://github.com/sphinx-gallery/sphinx-gallery/commits/master
   - https://github.com/sphinx-gallery/sphinx-gallery/releases/
   - https://sphinx-gallery.github.io/dev/changes.html


.. toctree::
   :maxdepth: 3
   
   0.7.0/0.7.0
