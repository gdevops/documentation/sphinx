.. index::
   pair: Sphinx ; gallery
   ! sphinx-gallery

.. _sphinx_gallery:

=======================================================================================
**sphinx-gallery** (Sphinx extension for automatic generation of an example gallery)
=======================================================================================

.. seealso::

   - https://x.com/lucyleeow
   - https://github.com/sphinx-gallery/sphinx-gallery
   - https://github.com/sphinx-gallery/sphinx-gallery/commits/master
   - https://github.com/sphinx-gallery/sphinx-gallery/graphs/contributors
   - https://sphinx-gallery.github.io/stable/index.html
   - https://github.com/sphinx-gallery/sample-project

.. toctree::
   :maxdepth: 3
   
   versions/versions
