.. include:: ../../sphinx_refs.txt

.. index::
   pair: Sphinx ; linux extensions
   pair: Table ; rest-flat-table
   ! LinuxDoc


.. _linux_extension:

==============================================================================
LinuxDoc Sphinx-doc extensions for sophisticated C developer (NEW, 2016-07)
==============================================================================


- https://github.com/return42/linuxdoc
- https://return42.github.io/linuxdoc/

Introduction
============

The **LinuxDoc library** contains sphinx-doc extensions and command
line tools to extract documentation from C/C++ source file comments.

**Even if this project started in context of the Linux-Kernel
documentation, you can use these extensions in common sphinx-doc
projects**.

LinuxDoc is hosted at github: https://github.com/return42/linuxdoc


.. _install_linux_doc:

Installation
=============

::

    pipenv install -e git+http://github.com/return42/linuxdoc.git#egg=master



If you are a developer and like to contribute to the LinuxDoc lib, fork on
github or clone and make a developer install::

    git clone https://github.com/return42/linuxdoc
    cd linuxdoc
    make install

Below you see how to integrate the LinuxDoc sphinx extensions into your sphinx
build process. In the ``conf.py`` (`sphinx config`_) add the LinuxDoc
extensions:

.. code-block:: python

    # https://return42.github.io/linuxdoc/install.html
    extensions = extensions + [
        "linuxdoc.rstFlatTable",  # Implementation of the 'flat-table' reST-directive.
        "linuxdoc.rstKernelDoc",  # Implementation of the 'kernel-doc' reST-directive.
        "linuxdoc.kernel_include",  # Implementation of the 'kernel-include' reST-directive.
        "linuxdoc.manKernelDoc",  # Implementation of the 'kernel-doc-man' builder
        "linuxdoc.cdomain",  # Replacement for the sphinx c-domain.
        "linuxdoc.kfigure",  # Sphinx extension which implements scalable image handling.
    ]


Flat table
===========

- :ref:`doc_formats:rst_flat_table`
