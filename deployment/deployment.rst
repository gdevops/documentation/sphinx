.. index::
   pair: Sphinx ; Deployment

.. _sphinx_deployment:

=======================
Sphinx deployment
=======================

- :ref:`tuto_doc:hebergeurs_sphinx`

.. toctree::
   :maxdepth: 3

   articles/articles
