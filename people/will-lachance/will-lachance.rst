.. index::
   pair: People ; Will Lachance 

.. _will_lachance:

=======================
**Will Lachance**
=======================

- https://github.com/wlach
- https://github.com/wlach.atom  |FluxWeb|

Bio
=====

Interested in technical communication and the energy transition.
