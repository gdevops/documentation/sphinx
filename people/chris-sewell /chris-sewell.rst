.. index::
   pair: People ; Chris Sewell  

.. _chris_sewell :

=======================
**Chris Sewell**
=======================

- https://github.com/chrisjsewell
- https://github.com/chrisjsewell.atom  |FluxWeb|

Bio
=====

Open source developer and materials researcher. 

Working towards open source, reproducible and shareable science solutions 😀

École Polytechnique Fédérale de Lausanne.


Projects
=============

- https://github.com/executablebooks/sphinx-design
- https://github.com/executablebooks/sphinx-design/releases.atom |FluxWeb|
- https://github.com/useblocks/sphinx-needs
- https://github.com/useblocks/sphinx-needs/releases.atom |FluxWeb|
- https://github.com/executablebooks/MyST-Parser
- https://github.com/executablebooks/MyST-Parser/releases.atom |FluxWeb|
