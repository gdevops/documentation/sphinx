.. index::
   pair: People ; Adam Turner 

.. _adam_turner:

=======================
**Adam Turner**
=======================

- https://github.com/AA-Turner
- https://github.com/AA-Turner/wlach.atom  |FluxWeb|

Bio
=====

- https://github.com/docutils/docutils
- https://sourceforge.net/p/docutils/code/HEAD/tree/trunk/
- https://www.docutils.org/

Python Core Developer and PEP Editor. 

**Maintainer of Sphinx and Docutils**.
